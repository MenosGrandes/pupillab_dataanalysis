from pandas import read_csv
from sys import exit
from pathlib import Path


class PupilLabCSVReader:
    def __init__(self, csvFile, filtered_cols=None, removeZeroDiameter: bool = True):
        try:
            self.data = read_csv(csvFile, usecols=filtered_cols)
        except FileNotFoundError:
            print("No such file {}".format(csvFile))
            exit(1)
        else:
            if removeZeroDiameter:
                self.__removemodel_confidence()
                self.__removeZeroLux()
                self.__removeZeroDiameter()

    def __removemodel_confidence(self):
        self.data = self.data[self.data.model_confidence == 1]

    def __removeZeroDiameter(self):
        self.data = self.data[self.data.diameter != 0]

    def __removeZeroLux(self):
        self.data = self.data[self.data.lux != 0]

    def filterEyeId(self, eye_id):
        return self.data.drop(self.data[self.data.eye_id == eye_id].index).drop(
            columns=["eye_id"]
        )

    def saveToExcel(self, eye_id):
        Path("EXPORTED").mkdir(parents=True, exist_ok=True)
        eyeIdData = self.filterEyeId(eye_id)
        print(f"{eyeIdData}")
        eyeIdData.to_excel(
            r"EXPORTED/{name}_eye.xlsx".format(name=eye_id), index=None, header=True
        )

    def saveToCSV(self, eye_id):
        Path("EXPORTED").mkdir(parents=True, exist_ok=True)

        self.filterEyeId(eye_id).to_csv(

            r"EXPORTED/{name}_eye.csv".format(name=eye_id), index=None, header=True

        )
