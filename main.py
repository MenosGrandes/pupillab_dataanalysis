from pupilLabCSVReader import PupilLabCSVReader
from pupilLabPlotter import PupilLabPlotter
import matplotlib.pyplot as plt

filter_columns = ["eye_id", "pupil_timestamp", "diameter", "lux", "model_confidence"]
plot_columns_x = ["pupil_timestamp"]
plot_columns_y = ["diameter", "lux"]

pupilLabCSV = PupilLabCSVReader(
    "CSV_DATA/pupil_positions.csv",
    filtered_cols=filter_columns,
    removeZeroDiameter=True,
)
pupilLabCSV.saveToExcel(0)
pupilLabCSV.saveToExcel(1)

first_eye = pupilLabCSV.filterEyeId(0)
second_eye = pupilLabCSV.filterEyeId(1)

first_eye_plotter = PupilLabPlotter(
    first_eye,
    plot_columns_x,
    plot_columns_y,
    synchronizedZoom=(True, False),
    title="Eye 0",
)
second_eye_plotter = PupilLabPlotter(
    second_eye,
    plot_columns_x,
    plot_columns_y,
    synchronizedZoom=(True, False),
    title="Eye 1",
)


plt.show()
