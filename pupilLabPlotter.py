from pandas import read_csv, DataFrame
from typing import List, Tuple
import matplotlib.pyplot as plt


class PupilLabPlotter:
    @property
    def x_columns(self):
        return self.__x_columns

    @x_columns.setter
    def x_columns(self, x_columns):
        self.__x_columns = x_columns

    @property
    def y_columns(self):
        return self.__y_columns

    @y_columns.setter
    def y_columns(self, y_columns):
        self.__y_columns = y_columns

    @property
    def plot_data(self):
        return self.__plot_data

    @plot_data.setter
    def plot_data(self, plot_data):
        self.__plot_data = plot_data

    def __init__(
        self,
        plot_data: DataFrame,
        x_columns: List[str],
        y_columns: List[str],
        title: str,
        synchronizedZoom: Tuple[bool, bool] = (False, False)
    ):
        self.__x_columns = x_columns
        self.__y_columns = y_columns
        self.__plot_data = plot_data
        self.__synchronizedZoom = synchronizedZoom
        self.__title = title
        self.plot()

    def plot(self):
        fig, axes = plt.subplots(
            nrows=len(self.__x_columns),
            ncols=len(self.__y_columns),
            sharex=self.__synchronizedZoom[0],
            sharey=self.__synchronizedZoom[1],
        )
        fig.suptitle(self.__title)
        axes_counter = 0
        for x in self.__x_columns:
            for y in self.__y_columns:
                self.__plot_data.plot(ax=axes[axes_counter], x=x, y=y)
                axes_counter += 1
